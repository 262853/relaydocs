Techsmith Relay
===============

!!! warning "Pozor"
    Program TS Relay není dlouhodobě podporován a aktualizován, jeho vývojáři nezaručují funkčnost na současných operačních systémech. Z tohoto důvodu **byla služba v srpnu 2023 ukončena**.
    Pro snadné pořízení záznamu přednášky na vlastním PC lze využít např. [záznam schůzky v MS Teams](https://it.muni.cz/sluzby/microsoft-teams/dalsi-informace/zaznamy-schuzek-v-ms-teams){:target="_blank"}.


![](home/asigraf_crop.png){:class="noshadow"}


